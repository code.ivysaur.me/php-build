# php-build

![](https://img.shields.io/badge/written%20in-bash-blue)

Script to build a win32 PHP build environment.

## Overview

It takes many steps to build PHP on windows using the native toolchain. May as well automate the process!

- `php-build` requires Cygwin; the final development environment does not require cygwin, nor do any final binaries
- Build using any VS installation
- Build from any git branch
- Uses `jom` for parallel build on VS2012 or older

## Usage

Configure the build environment by modifying the supplied `php-build.configuration.sh` file. Then use `php-build.sh` to compile PHP.


```
Usage:
  php-build.sh [options]

Options:
  --cmd             Open cmd.exe terminal in src dir with correct environment
  --explorer        Open explorer in src dir with correct environment
  --force           Delete php-sdk directory if it already exists
  --help            Display this message

Examples:
  - Build PHP, but fail if destination directory already exists:
    ./php-build.sh

  - Build PHP, removing destination directory if it already exists:
    ./php-build.sh --force
```


## TODO

- Build all dependencies instead of using dependency zipfiles from windows.php.net

## See Also

- Official build instructions: https://wiki.php.net/internals/windows/stepbystepbuild 
- A similar project: https://github.com/johmue/win-php-sdk-builder
- Comments re `nmake snap` issue on Win64: http://marc.info/?l=php-windows&m=124178988616621


## Download

- [⬇️ php-build_r21.tar.gz](dist-archive/php-build_r21.tar.gz) *(10.00 KiB)*
